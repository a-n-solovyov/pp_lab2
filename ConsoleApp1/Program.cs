﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Versioning;
using System.Threading;
using System.Threading.Tasks;
using static ConsoleApp1.SupportMethods;
using static ConsoleApp1.Extensions;

namespace ConsoleApp1
{
    /*
     *1. Реализовать последовательную функцию агрегирования (сумма, произведение и т.п.)
     * предварительно обработанных (любая операция с ощутимым временем выполнения) элементов вектора.
    2. Сделать параллельную реализацию данной функции.
    3. Сделать параллельную реализацию данной функции с использованием механизмов синхронизации
     в зависимости от варианта (см. ниже).
    4. Результат представить в виде таблиц, содержащей данные о количестве элементов вектора, 
    типе реализации, времени исполнения и результате выполнения функции.
     b. Mutex
    c. Semaphore
     */
    public static class Program
    {
        private static int N = 10;
        public static readonly int TestCount = 10;
        private static readonly Func<int, double> FuncWithSomeTime = i => Math.Sin(i);

        public delegate double TestMethodDelegate(Test testClass);

        static void Main(string[] args)
        {
            Console.WriteLine(Environment.ProcessorCount);
            Console.WriteLine(Assembly
                .GetEntryAssembly()?
                .GetCustomAttribute<TargetFrameworkAttribute>()?
                .FrameworkName);


            int[] nArr = {10, 100, 1000, 10000};
            Console.WriteLine($"{"threads\\N",10}{nArr[0],10}{nArr[1],10}{nArr[2],10}{nArr[3],10}");

            Console.WriteLine("Sum sequential");
            PrintResults(nArr, Sum, sequential: true);
            Console.WriteLine("Sum parallel");
            PrintResults(nArr, SumParallel);
            Console.WriteLine("Sum semaphore");
            PrintResults(nArr, SumSemaphore);
            Console.WriteLine("Sum mutex");
            PrintResults(nArr, SumMutex);

            // Test
            CheckSum();
        }

        private static void CheckSum()
        {
            var arr = new int[10];
            var test = new Test(2, arr);
            test.FillArray();

            double result = Sum(test);
            double parallelResult = SumParallel(test);
            double semaphoreResult = SumSemaphore(test);
            double mutexResult = SumMutex(test);

            if (result != parallelResult || result != semaphoreResult || result != mutexResult)
            {
                Console.WriteLine($"Test failed: {result}, parallel: {parallelResult}, semaphore: {semaphoreResult}");
            }
        }

        public static double Sum(Test testClass)
        {
            double sum = 0;
            for (int i = 0; i < testClass.Array.Length; i++)
            {
                sum += FuncWithSomeTime(testClass.Array[i]);
            }

            return sum;
        }

        public static double SumParallel(Test testClass)
        {
            double sum = 0;
            Parallel.For(0, testClass.Array.Length, new ParallelOptions(){
                    MaxDegreeOfParallelism = testClass.ThreadCount
            },
            i => { sum += FuncWithSomeTime(testClass[i]); });

            return sum;
        }

        public static double SumMutex(Test testClass)
        {
            Mutex mutex = new Mutex();
            double sum = 0;
            Thread[] threads = new Thread[testClass.ThreadCount];

            void ProcessPart(int start, int take)
            {
                int max = start + take;
                for (int i = start; i < max; i++)
                {
                    var val = FuncWithSomeTime(testClass.Array[i]);
                    mutex.WaitOne();
                    //Console.WriteLine($"{Thread.CurrentThread.Name} - {val}");
                    sum += val;
                    mutex.ReleaseMutex();
                }
            }

            for (int i = 0, start = 0, take = testClass.Array.Length / testClass.ThreadCount;
                i < testClass.ThreadCount;
                i++, start += take)
            {
                var start1 = start;
                threads[i] = new Thread(() => ProcessPart(start1, take));
            }

            threads.StartAndJoin();

            return sum;
        }

        public static double SumSemaphore(Test testClass)
        {
            Semaphore semaphore = new Semaphore(1, 1);
            double sum = 0;
            Thread[] threads = new Thread[testClass.ThreadCount];

            void ProcessPart(int start, int take)
            {
                int max = start + take;
                for (int i = start; i < max; i++)
                {
                    var val = FuncWithSomeTime(testClass.Array[i]);
                    semaphore.WaitOne();
                    //Console.WriteLine($"{Thread.CurrentThread.Name} - {val}");
                    sum += val;
                    semaphore.Release();
                }
            }

            for (int i = 0, start = 0, take = testClass.Array.Length / testClass.ThreadCount;
                i < testClass.ThreadCount;
                i++, start += take)
            {
                var start1 = start;
                threads[i] = new Thread(() => ProcessPart(start1, take));
            }

            threads.StartAndJoin();

            return sum;
        }
    }


    public class Test
    {
        public Test(int elementCount, int threadCount)
        {
            ElementCount = elementCount;
            ThreadCount = threadCount;
            Array = new int[elementCount];
            FillArray();
        }

        public Test(int threadCount, int[] array) : this(array?.Length ?? 0, threadCount)
        {
            Array = array ?? new int[] { };
            ElementCount = Array.Length;
            ThreadCount = threadCount;
        }

        public void FillArray()
        {
            Random random = new Random();
            for (int i = 0; i < Array.Length; i++)
            {
                Array[i] = random.Next();
            }
        }

        public int ThreadCount { get; }

        public int ElementCount { get; }

        public int[] Array { get; }

        public int Count => ElementCount;

        public int this[int i] => Array[i];
    }
}