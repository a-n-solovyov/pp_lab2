using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ConsoleApp1
{
    public class SupportMethods
    {
        public static void PrintResults(int[] nArr, Program.TestMethodDelegate test, bool sequential = false)
        {
            int[] mArr = sequential ? new[]{1} : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            
            long[,] result = new long[mArr.Length, nArr.Length + 1];

            for (int i = 0; i < mArr.Length; i++)
            {
                result[i, 0] = mArr[i];
            }
            
            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 1; j < result.GetLength(1); j++)
                {
                    result[i, j] = MultipleRun(test, new Test(elementCount: nArr[j-1], threadCount: mArr[i]));
                }
            }
            Print2DArray(result);
        }
        
        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write($"{matrix[i,j],10}");
                }
                Console.WriteLine();
            }
        }

        public static void PrintHeader(int[] nArr)
        {
            for (int i = 0; i < nArr.GetLength(0); i++)
            {
                Console.Write($"{nArr[i],10}");
            }
            Console.WriteLine("\n--------------------------------------------");
        }
        
        private static long MultipleRun(Program.TestMethodDelegate @delegate, Test testClass)
        {
            double avg = 0;
            for (int i = 0; i < Program.TestCount; i++)
            {
                avg += MeasureTime(@delegate, testClass);
            }

            return (int) (avg / Program.TestCount);
        }

        private static long MeasureTime(Program.TestMethodDelegate @delegate, Test testClass)
        {
            var s = Stopwatch.StartNew();
            @delegate(testClass);
            s.Stop();
            return s.ElapsedTicks;
        }
    }

    public static class Extensions
    {
        public static void StartAndJoin(this IEnumerable<Thread> threads)
        {
            var enumerable = threads as Thread[] ?? threads.ToArray();
            NameThreads(enumerable);
            foreach (var thread in enumerable) thread.Start();
            foreach (var thread in enumerable) thread.Join();
        }
        
        public static void NameThreads(IEnumerable<Thread> threads)
        {
            int i = 0;
            foreach (var thread in threads) 
                thread.Name = $"Thread #{++i}";
        }
    }
}